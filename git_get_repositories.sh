#!/bin/bash

# You should list git submodules repositories in SUBMODULES variable here
# i.e.
# SUBMODULES="https://gitservera.com/groupa/project-a.git"
# SUBMODULES+=" https://gitserverb.com/groupb/project-b.git"

SUBMODULES="https://gitlab.com/fredloreaud/osef-posix.git"
