#include "SysLED.h"

#include <unistd.h> // getopt
#include <iostream> // cout
//#include <cstdlib>
//#include <iomanip> // setw

bool getLedIdState(int argc, char** argv, uint32_t& id, bool& state)
{
    bool ret = true;    
    int32_t opt = -1;
    uint64_t tmp = 0;

    if(argc>1)
    {  
	/*use function getopt to get the arguments with option."hu:p:s:v" indicate 
	that option h,v are the options without arguments while u,p,s are the
	options with arguments*/
	while((opt=getopt(argc,argv,"l:s:"))!=-1)
	{
	    switch(opt)
	    {
		case 'l':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if(tmp<=0xffUL)
                    {
                        id = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
		    break;
                case 's':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    switch(tmp)
                    {
                        case 0:
                            state = false;
                            break;
                        case 1:
                            state = true;
                            break;
                        default:
                            ret = false;
                            break;
                    }
                    break;
		default:
                    ret = false;
                    break;
	    }
	}
    }
    
    if(!ret)
    {
        std::cout<<"Usage:   "<<*argv<<" [-option] [argument]"<<std::endl;
        std::cout<<"option:  "<<std::endl;
        std::cout<<"         "<<"-l  LED Id [0-255]"<<std::endl;
        std::cout<<"         "<<"-s  LED state [0-1]"<<std::endl;
    }
    
    return ret;
}

int main(int argc, char** argv)
{
    int ret = -1;
    
    uint32_t id = 0;
    bool state = false;
    if(getLedIdState(argc, argv, id, state))
    {
        OSEF::SysLED led(id);
        if(led.setState(state))
        {
            ret = 0;
        }
    }
    
    return ret;
}

