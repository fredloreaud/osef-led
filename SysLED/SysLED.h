#ifndef OSEFSYSLED_H
#define OSEFSYSLED_H

#include <stdint.h>  // uint32_t
#include <string>

namespace OSEF {

const std::string LED_PATH = "/sys/class/leds";

class SysLED {
public:
    explicit SysLED(const uint32_t& led, const std::string& family = "led");
    virtual ~SysLED();

    bool on() const;
    bool off() const;

    bool setState(const bool& s);

    uint32_t getId() const {return id;}

private:
    SysLED(const SysLED& orig);
    SysLED& operator=(const SysLED& orig);

    std::string getBrightnessFile() const;

    std::string getLedPath() const;

    uint32_t id;
    std::string prefix;
};
}  // namespace OSEF

#endif /* OSEFSYSLED_H */
