#include "SysLED.h"
#include "Debug.h"
#include "File.h"

#include <fcntl.h>  // open
#include <unistd.h>  // write

OSEF::SysLED::SysLED(const uint32_t& led, const std::string& family)
    : id(led)
    , prefix(family){}

OSEF::SysLED::~SysLED() {}

bool OSEF::SysLED::setState(const bool& s)
{
    bool ret = false;

    if(s)
    {
        ret = on();
    }
    else
    {
        ret = off();
    }

    return ret;
}

bool OSEF::SysLED::on() const
{
    const bool ret = OSEF::File::overwriteString(getBrightnessFile(), "1");
    return ret;
}

bool OSEF::SysLED::off() const
{
    const bool ret = OSEF::File::overwriteString(getBrightnessFile(), "0");
    return ret;
}

std::string OSEF::SysLED::getBrightnessFile() const
{
    std::string str = getLedPath();
    str += "/brightness";
    return str;
}

std::string OSEF::SysLED::getLedPath() const
{
    std::string str = LED_PATH + "/" + prefix + std::to_string(id);
    return str;
}
