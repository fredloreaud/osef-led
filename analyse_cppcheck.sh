#!/bin/bash

# https://blog.kitware.com/static-checks-with-cmake-cdash-iwyu-clang-tidy-lwyu-cpplint-and-cppcheck/

# http://cppcheck.net/manual.html#CMake

mkdir -p build/CppCheck
cd build/CppCheck
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ../..
cppcheck --error-exitcode=-1 --enable=all --suppressions-list=../../analyse_cppcheck_suppressions.txt --project=compile_commands.json
