#!/bin/bash

# You should list sources in ANALYSIS_SOURCES variable here
# i.e.
# ANALYSIS_SOURCES="SrcA/*.h SrcA/*.cpp"
# ANALYSIS_SOURCES+=" SrcB/*.h SrcB/*.cpp"

ANALYSIS_SOURCES="SysLED/*.h SysLED/*.cpp"
