#!/bin/bash

# runtime/references is discarded as passing a pointer is more prone to errors ...
FILTER=-runtime/references

# runtime/string forbids static strings then decrease readability and runtime performances when string needs to be constructed from char*
FILTER+=,-runtime/string

# runtime/arrays merely checks array size begin with letter k notit really is a constant ...
FILTER+=,-runtime/arrays

# build/header_guard should check uniqueness and not a cumbersome pattern
FILTER+=,-build/header_guard

# legal/copyright is redundant with repo LICENSE file
FILTER+=,-legal/copyright

# whitespace/newline doesn't allow else statement as same level as corresponding if ...
FILTER+=,-whitespace/newline

# whitespace/line_length 80 characters limit is obsolete for some time already ...
FILTER+=,-whitespace/line_length

# whitespace/parens requires space after if statement which doesn't improve readability
FILTER+=,-whitespace/parens

# whitespace/braces requires opening brace at end of line which does not increase readability
FILTER+=,-whitespace/braces

# whitespace/indent doesn't support indent after #ifdef
FILTER+=,-whitespace/indent

# whitespace/comments doesn't check 2 spaces between code and C style /**/ comments
#FILTER+=,-whitespace/comments

# whitespace/tab doesn't allow #define indentation
#FILTER+=,-whitespace/tab

# whitespace/operators only checks one space around operator and doesn't check +=
#FILTER+=,-whitespace/operators

# get analysis sources
. ./analyse_get_cpplint_sources.sh

# use complete repo sources if specific folder name is not provided
if [ -z "$1" ]
then
    SOURCES=$ANALYSIS_SOURCES
else
    SOURCES="$1/*.cpp $1/*.h"
fi

if [ -z "$SOURCES" ]
then
    echo "No source provided. Please provide a directory as argument or fill ANALYSIS_SOURCES variable in analyse_get_sources.sh"
else
    cpplint --filter=$FILTER $SOURCES
fi
