cmake_minimum_required(VERSION 3.13)

include(osef.cmake)

project(OSEF_LED)

add_subdirectory(osef-posix/File)

add_library(${PROJECT_NAME} STATIC)

target_sources(${PROJECT_NAME}
    PRIVATE
        SysLED/SysLED.cpp
)

target_include_directories(${PROJECT_NAME}
    PUBLIC
        SysLED
)

target_link_libraries(${PROJECT_NAME}
    OSEF_File
)